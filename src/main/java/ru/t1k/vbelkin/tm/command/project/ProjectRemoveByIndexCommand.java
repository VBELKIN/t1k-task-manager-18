package ru.t1k.vbelkin.tm.command.project;

import ru.t1k.vbelkin.tm.model.Project;
import ru.t1k.vbelkin.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-remove-by-index";

    public static final String DESCRIPTION = "Remove project by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        getProjectTaskService().removeProjectById(project.getId());
    }

}
