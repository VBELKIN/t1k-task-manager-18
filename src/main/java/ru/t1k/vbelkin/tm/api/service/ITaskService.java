package ru.t1k.vbelkin.tm.api.service;

import ru.t1k.vbelkin.tm.enumerated.Sort;
import ru.t1k.vbelkin.tm.enumerated.Status;
import ru.t1k.vbelkin.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    Task add(Task task);

    List<Task> findAll(Sort sort);

    List<Task> findAll(Comparator comparator);

    Task create(String name);

    Task create(String name, String description);

    void clear();

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusById(String id, Status status);

}
